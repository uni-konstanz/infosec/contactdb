#!/usr/bin/env python3

import ipaddress
import sqlite3
import csv
import sys


class record(object):
	def __init__(self, header, row):
		for i in range(len(header)):
			setattr(self, header[i], row[i].strip())
	
	def __str__(self):
		return str(self.__dict__)


def readcsv(fname):
	handle = open(fname)
	reader = csv.reader(handle)
	header = next(reader)
	#record = type('record', (object, ), { dict.fromkeys(header)})
	records = []
	for row in reader:
		records.append(record(header, row))
	handle.close()
	return records


def check_records(records, ip):
	ip = ipaddress.IPv4Address(ip)
	longest_prefix = 0
	found = None
	for r in records:
		if r.net:
			net = ipaddress.IPv4Network(r.net)
			if ip in net and net.prefixlen > longest_prefix:
				found = r
				longest_prefix = net.prefixlen
	return found


def usage():
	print (sys.argv[0] + " <contacts> <ipaddress>")
	print ("Find network and contact for ip address.")
	print ("\tcontacts: path to intput file (.csv)")
	print ("\tipaddress: the ip address to check")
	sys.exit(2)


def main(argv):
	if len(argv)<2:
		usage()
	fname=argv[0]
	ip=argv[1]
	records = readcsv(fname)
	return check_records(records, ip)


if __name__ == '__main__':
	found = main(sys.argv[1:])
	print(found)
		
