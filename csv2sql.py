#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###
# Copyright (c) 2020 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

import csv
import os
import sys
import getopt
import sqlite3

try:
	import dateutil.parser as dateparse
except (ImportError, ModuleNotFoundError) as e:
	print (e)
	import pip
	pip.main(['install', '--user', 'python-dateutil'])
	import dateutil.parser as dateparse


class record(tuple):
	def __new__(cls, iterable):
		return super(record, cls).__new__(cls, tuple(iterable))

	def __init__(self, iterable):
		pass


class table(list):
	def __init__ (self, name, header, *iterable):
		super().__init__(self)
		self.__name = name
		self.__header = header
		if iterable:
			list.extend(self, *iterable)

	@property
	def name(self):
		return self.__name

	@property
	def header(self):
		return self.__header

	@property
	def records(self):
		return list(self)


def csv2table(fname):
	handle = open(fname)
	reader = csv.reader(handle)
	name = os.path.splitext(os.path.basename(fname))[0]
	header = next(reader)
	tab = table(name, header)
	for row in reader:
		tab.append(record(row))
	handle.close()
	return tab


def table2sql(tab):
	sql = []
	create_table_sql = "CREATE TABLE %s (%s);" % (tab.name, str(tab.header)[1:-1])
	sql.append(create_table_sql)
	for rec in tab.records:
		insert_sql = "REPLACE INTO %s VALUES %s;" %(tab.name, rec)
		sql.append(insert_sql)
	return sql


def execute_sql(db, sqlscript=[]):
	for statement in sqlscript:
		try:
			db.execute(statement)
		except sqlite3.OperationalError as e:
			print ("ERROR Executing: %s\n%s." % (statement, e), file=sys.stderr)
	db.commit()


def usage():
	print (sys.argv[0] + " <-f infile> [-d database]")
	print ("Parse csv file and convert to sql script.")
	print ("\t-f|--file: path to intput file (.csv)")
	print ("\t-d|--db: path to sqlite3 database")
	sys.exit(2)


class optarg():
	def __init__(self, argv, usage):
		self.__argv = argv
		self.__file = None
		self.__db = None

		try:
			opts, args = getopt.getopt(argv, "hf:d:", ["help", "file=", "db="])
			for opt, arg in opts:
				if opt in ("-h", "--help"):
					usage()
				elif opt in ("-f", "--file"):
					if not os.path.isfile(arg):
						print ("ERROR: '%s' not found" % arg, file=sys.stderr)
						usage()
					self.__file = arg
				elif opt in ("-d", "--db"):
					if not os.path.isfile(arg):
						print ("WARN: '%s' not found, creating new" % arg, file=sys.stderr)
					self.__db = arg
		except getopt.GetoptError:
			usage()
		if not self.__file:
			print ("ERROR: missing argument -f <file>", file=sys.stderr)
			usage()

	@property
	def db(self):
		return self.__db

	@property
	def file(self):
		return self.__file


def main(argv):
	optargs = optarg(argv, usage)

	tab = csv2table(optargs.file)
	sql = table2sql(tab)

	if optargs.db:
		db_conn = sqlite3.connect(optargs.db)
		execute_sql(db_conn, sql)
		db_conn.close()

	return sql


if __name__ == '__main__':
	sql = main(sys.argv[1:])
	for statement in sql:
		print(statement)

